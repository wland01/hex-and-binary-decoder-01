import java.util.Scanner;

public class HexAndBinaryDecoder {

    public static long hexStringDecode(String hex) {
        char[] hexChars;
        int i;
        long sum;

        hex = hex.toUpperCase();        //self explanatory statements

        hex = hex.replace("0X", "");

        hexChars = hex.toCharArray();   //converts to character array

        sum = hexCharDecode(hexChars[0]);   //gets first value for sum

        if (hexChars.length > 1) {
            for (i = 1; i < hexChars.length; ++i) {
                sum = (sum * 16) + hexCharDecode(hexChars[i]);  //decodes each char and returns a value for the sum algorithm
            }
        }

        return sum;
    } //Decode whole hex string
    public static short hexCharDecode(char hexChar) {
        char[] hexLetters = {'A','B','C','D','E','F'};
        char[] hexNumbers = {'0','1','2','3','4','5','6','7','8','9'};
        int i;
        int j;
        short hexVal = 1;

        hexChar = Character.toUpperCase(hexChar);   //makes sure it is uppercase for ease

        for (i=0; i<6; ++i) {
            if (hexChar == hexLetters[i]) {
                hexVal = (short) (i + 10);
            }
        }
        for (j=0; j<10; ++j) {
            if (hexChar == hexNumbers[j]) {
                hexVal = (short) (j);
            }
        }
        return hexVal;
    } //Decode single character
    public static long binaryStringDecode(String bin) {
        char[] binaryChars;
        int i;
        long sum = 0;

        bin = bin.toUpperCase();        //self explanatory statements

        bin = bin.replace("0B", "");

        binaryChars = bin.toCharArray();    //converts to character array

        for (i = binaryChars.length-1; i > -1; --i) {

            if (binaryChars[i] == '1') {        //converts binary to powers of base 2
                sum = sum + (long)(Math.pow(2,(Math.abs(i-(binaryChars.length-1)))));
            }
        }
        return sum;
    } //Decode binary string
    public static String binaryToHex(String bin) {
        String[] binStrings = new String[99];
        String[] binaryToHex = {"0", "1", "10", "11", "100", "101",             //two string arrays
                "110", "111", "1000", "1001", "1010", "1011",
                "1100", "1101", "1110", "1111"};
        String[] hexEquivalent = {"0","1","2","3","4","5","6","7","8"
                ,"9","A","B","C","D","E","F"};
        int i;
        int j = 0;
        int k;
        int compare;
        String result = "";

        for (i = 0; i < bin.length(); i=i+4) {
            binStrings[j] = bin.substring(i,i+4);
            for (k = 0; k < 3; ++k) {
                if (binStrings[j].charAt(0) == '0') {           //splits user string into sets of four
                    binStrings[j] = binStrings[j].substring(k+1,binStrings[j].length());
                }
            }
            for (compare = 0; compare < 16; ++compare) {        //converts each string into hex equivalent
                if (binStrings[j].equals(binaryToHex[compare])) {
                    binStrings[j] = hexEquivalent[compare];
                    result = result.concat(binStrings[j]);      //adds hex equivalents
                }
            }
            ++j;
        }
        return result;
    } //Optional


    public static void main(String[] args) {
        Scanner scnr = new Scanner(System.in);
        int menuInput = 0;  // user menu choice
        String hexString;   //user entered strings
        String binaryString;    //
        long hexDecodeResult;   //result of method calls
        long binaryDecodeResult;
        String binaryToHexResult;

        while (menuInput != 4) {    //breaks loop if user enters 4
            System.out.println("Choose an option:");
            System.out.println("1. Decode a hex string.");
            System.out.println("2. Decode a binary string.");       //menu
            System.out.println("3. Convert binary to hex.");
            System.out.println("4. Quit.");
            menuInput = scnr.nextInt();     //takes input for menu

            if (menuInput == 1) {       //if user enters 1, sends user hex string to methods and returns with result
                System.out.println("Please enter the hex string:");
                hexString = scnr.next();
                hexDecodeResult = hexStringDecode(hexString);
                System.out.println("Result: "+ hexDecodeResult);
            }

            if (menuInput == 2) {       //if user enters 2, sends user binary string to methods and returns with result
                System.out.println("Please enter the binary string:");
                binaryString = scnr.next();
                binaryDecodeResult = binaryStringDecode(binaryString);
                System.out.println("Result: "+ binaryDecodeResult);
            }
            if (menuInput == 3) {       //if user enters 2, sends user binary string to methods and returns with result
                System.out.println("Please enter the binary string:");
                binaryString = scnr.next();
                binaryToHexResult = binaryToHex(binaryString);
                System.out.println("Result: "+ binaryToHexResult);
            }
        }
        System.out.println("Quitting program.");    //text if user enters 4
    }
}
